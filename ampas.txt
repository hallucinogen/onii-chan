
// optional
Get All Game
<url>/get_all_game
return {
  [{
    
  }]
}

--------------------------------------------------------------------------------------------

Get All Room
<url>/get_all_room
return {
  [{
    roomID: 1
  }, {
    roomID: 2
  }, {
    roomID: 3
  }, {
    roomID: 4
  }]
}

--------------------------------------------------------------------------------------------

Get Room Info
<url>/get_room_info?roomID=1
return {
  roomID: 1,
  maxPlayer: 4,
  players: [{
    playerID: 13508074
  }, {
    playerID: 13508103
  }],
  owner: 13508074,
  // optional
  gameType: 1,
  gameState: "x---xx",
  playerTurn: 13508074,
  isFinished: false
}

--------------------------------------------------------------------------------------------

Enter Room
// gw sotoy soal GCM
<url>/enter_room?roomID=1&playerID=13508103&GCMID=APTX4869
return {
  // fails
}

OR

return {
  roomID: 1,
  player: 13508103,
  GCMID: APTX4869
}
// update room info
// and then broadcast to all connected player (with given GCMID) current room info

--------------------------------------------------------------------------------------------

Start Game
<url>/start_game?gameID=1
return {
  // ga tau fail kalo kondisinya gmn
  message: 'fail'
}

return {
  message: 'success'
}

--------------------------------------------------------------------------------------------

Get Room Between Two
<url>get_room?playerID1=13508074&playerID2=13508103
return {
  roomID: 1, // kalo lom ada, maka baru. Kalo dah ada, maka yang lama
  turn: 13508074,
  // pilih
  gameState: {
    // kita pake ini :3
  }
}

--------------------------------------------------------------------------------------------

Turn Message
// no request, this should be given via GCM
return {
  roomID: 1,
  turn: 13508074,
  // pilih
  gameState: {
    // kita pake ini :3
  }
  // atau
  lastMove: {
  
  }
}
// set local game state as "turnStarted"
// if turn != our player, set local game state as "turnFinished"
// else, keep "turnStarted"

--------------------------------------------------------------------------------------------

Move message
<url>/move?roomID=1&playerID=13508103&move=???
<url>/move?roomID=1&playerID=13508103&gameState=????

return {
  message: 'success'
}

return {
  message: 'fail'
}

// then update game state
// set local game state as "turnFinished"
// then broadcast turn message to all player

--------------------------------------------------------------------------------------------

// Misc
Get Player Info
<url>/get_player_info?playerID=13508074
return {
  playerID: 13508074,
  username: Ampas,
  rating: 2200
}

--------------------------------------------------------------------------------------------

Get Friends
<url>/get_friends?playerID=13508074
return {
  playerID: 13508074,
  friends: [{
    playerID: 13508103
  }, {
    playerID: 13508056
  }]
}


URUTAN
1. Pilih player
2. Kirim myPlayerID dan enemyPlayerID => get new room or last played room
3. Client request room info periodically
4. Client kirim moveMessage ke server, server balikin "success" atau "fail"
5. Kalo menang, client suruh kirim "isFinished"
6. New room dibuat oleh yang kalah


http://localhost:8888/_ah/api/playerendpoint/v1/player (kalo mau update, pake PUT, kalo get pake player/{id}, insert pake player)
http://127.0.0.1:8888/_ah/api/discovery/v1/apis/playerendpoint/v1/rest (buat ngecek API)

http://localhost:8888/_ah/admin/datastore?&kind=Room&order=mPlayers (data store)

di https://onii-chan.appspot.com/_ah/api/playerendpoint/v1/player harus ada "content-type: application/json"

GCM sample send to all device:
URL:
	https://android.googleapis.com/gcm/send
Header:
	Content-Type: application/json
	Authorization: key=AIzaSyAqb0VZ741feSYRpg2CbxDjmFznqwsaXD4
Content:
	{
		"registration_ids": [
			"APA91bGMqfKvxi3HUe6YHRqTelB-44IkGNVMGYmdmAOQlv-DsQgl6COxsdQNgoSE0YES3l8wBJwo0C3nAq9XM98VDEvl5e7Ep9jjRXqoBTOYQdeu2KaoubwUwwRuWxpm_c_Z_-sSnxHvMogi0cnkkxgzQxc-BFVgsA"
		],
		"data": {
			"gogo": "ganteng"
		}
		
	}