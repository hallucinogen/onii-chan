package net.gogo.server.onii.model.helper;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

public final class PMF {

	private PMF() {}

	public static PersistenceManagerFactory getInstance() {
		if (sPMFInstance == null) {
			sPMFInstance = JDOHelper.getPersistenceManagerFactory("transactions-optional");
		}

		return sPMFInstance;
	}

	private static PersistenceManagerFactory sPMFInstance;
}
