package net.gogo.server.onii.model;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Player {

	public Player(String username) {
		this(username, 0);
	}

	public Player(String username, int rating) {
		mUsername = username;
		mRating = rating;
	}

	public static String getExistingPlayerByUsername(Player player) {
		return String.format("mUsername == \"%s\"", player.getUsername());
	}

	/**
	 * Getter / setter
	 */
	public Integer getRating()		{	return mRating;		}
	public String getUsername()		{	return mUsername;	}
	public Long getPlayerID()		{	return mPlayerID;	}
	public String getGCMID()		{	return mGCMID;		}

	public void setRating(int rating)			{	mRating 	= rating;		}
	public void setUsername(String username)	{	mUsername 	= username;		}
	public void setGCMID(String gcmID)			{	mGCMID		= gcmID;		}

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long mPlayerID;
	@Persistent String mUsername;
	@Persistent Integer mRating;
	@Persistent String mGCMID;
}
