package net.gogo.server.onii.model;

import java.util.ArrayList;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Room {

	// game state is JSON formatted string
	public Room(String gameState, int maxPlayer, long owner, ArrayList<Long> players) {
		this(gameState, maxPlayer, owner, players, GAME_TYPE_TIC_TAC_TOE);
	}

	public Room(String gameState, int maxPlayer, long owner, ArrayList<Long> players, int gameType) {
		mGameState = gameState;
		mGameType = gameType;
		mMaxPlayer = maxPlayer;
		mOwner = owner;
		mPlayers = players;
		mFinished = false;
		mPlayerTurn = owner;
	}

	public void addPlayer(Long playerID) {
		if (mPlayers == null) {
			mPlayers = new ArrayList<Long>();
		}
		mPlayers.add(playerID);
	}

	public static String getExistingCoupleQueryFilter(Long playerID1, Long playerID2) {
		return String.format("mPlayers.contains(%s) && mPlayers.contains(%s) && mFinished == false", playerID1, playerID2);
	}

	public static String getRoomWithPlayerTurnQueryFilter(Long playerID) {
		return String.format("mPlayers.contains(%s) && mPlayerTurn == %s", playerID, playerID);
	}

	/**
	 * Getter / setter
	 */
	public Long getRoomID()				{	return mRoomID;		}
	public Integer getMaxPlayer()		{	return mMaxPlayer;	}
	public ArrayList<Long> getPlayers()	{	return mPlayers;	}
	public Long getOwner()				{	return mOwner;		}
	public Integer getGameType()		{	return mGameType;	}
	public String getGameState()		{	return mGameState;	}
	public Long getPlayerTurn()			{	return mPlayerTurn;	}
	public Boolean isFinished()			{	return mFinished;	}

	public void setRoomID(long roomID)				{	mRoomID		= roomID;		}
	public void setMaxPlayer(int maxPlayer)			{	mMaxPlayer	= maxPlayer;	}
	public void setPlayers(ArrayList<Long> players)	{	mPlayers	= players;		}
	public void setOwner(long owner)				{	mOwner		= owner;		}
	public void setGameType(int gameType)			{	mGameType 	= gameType;		}
	public void setGameState(String gameState)		{	mGameState 	= gameState;	}
	public void setPlayerTurn(long turn)			{	mPlayerTurn	= turn;			}
	public void setFinished(boolean finished)		{	mFinished	= finished;		}

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long mRoomID;

	@Persistent private Integer mMaxPlayer;
	@Persistent private ArrayList<Long> mPlayers;
	@Persistent private Long mOwner;
	@Persistent private Integer mGameType;
	@Persistent private String mGameState;
	@Persistent private Long mPlayerTurn;
	@Persistent private Boolean mFinished;

	public static final int GAME_TYPE_TIC_TAC_TOE = 1;
}
