package net.gogo.server.onii.api;

import net.gogo.server.onii.model.Player;
import net.gogo.server.onii.model.Room;
import net.gogo.server.onii.model.helper.PMF;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(name = "tictachead", namespace = @ApiNamespace(ownerDomain = "gogo.net", ownerName = "gogo.net", packagePath = "server.onii.api"))
public class RoomEndpoint {

	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listRoom")
	public CollectionResponse<Room> listRoom(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Room> execute = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Room.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}

			execute = (List<Room>) query.execute();
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (Room obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<Room> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the java bean.
	 * @return The entity with primary key id.
	 */
	@ApiMethod(name = "getRoom")
	public Room getRoom(@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
		Room room = null;
		try {
			room = mgr.getObjectById(Room.class, id);
		} finally {
			mgr.close();
		}
		return room;
	}

	@ApiMethod(name = "getRoomsWithPlayerTurn", path = "room/player/{id}", httpMethod = "GET")
	public CollectionResponse<Room> getRoomsWithPlayerTurn(@Named("id") Long playerID) {
		PersistenceManager mgr = getPersistenceManager();
		List<Room> result = null;

		try {
			Query query = mgr.newQuery(Room.class);

			String filter = Room.getRoomWithPlayerTurnQueryFilter(playerID);
			query.setFilter(filter);

			result = (List<Room>) query.execute();
		} finally {
			mgr.close();
		}
		return CollectionResponse.<Room> builder().setItems(result).build();
	}

	/**
	 * This method gets room of two player. Create new room if there's no existing unfinised room
	 * @param playerID1 asker player
	 * @param playerID2 target player
	 * @return new room / existing unfinished room
	 */
	@ApiMethod(name = "getCoupleRoom", httpMethod = "GET")
	public Room getCoupleRoom(@Named("playerID1") Long playerID1, @Named("playerID2") Long playerID2) {
		PersistenceManager mgr = getPersistenceManager();
		Room room = null;
		try {
			Query query = mgr.newQuery(Room.class);
			String filter = Room.getExistingCoupleQueryFilter(playerID1, playerID2);
			System.out.println(filter);
			query.setFilter(filter);

			List<Room> rooms = (List<Room>)query.execute();
			if (rooms.size() > 0) {
				room = rooms.get(0);
			} else {
				ArrayList<Long> players = new ArrayList<Long>();
				players.add(playerID1);
				players.add(playerID2);

				JSONArray array = new JSONArray();
				for (int i = 0; i < 9; ++i) {
					array.put(Long.valueOf(0));
				}
				room = new Room(array.toString(), 2, playerID1, players);
				mgr.makePersistent(room);
			}
		} finally {
			mgr.close();
		}
		return room;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param room the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertRoom")
	public Room insertRoom(Room room) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (room.getRoomID() != null && containsRoom(room)) {
				throw new EntityExistsException("Object already exists");
			}
			mgr.makePersistent(room);
		} finally {
			mgr.close();
		}
		return room;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param room the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateRoom")
	public Room updateRoom(Room room) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsRoom(room)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(room);

			// tell everyone in the room that the room is changing
			ArrayList<Long> playerIDs = room.getPlayers();
			ArrayList<Player> players = new ArrayList<Player>();
			for (Long playerID : playerIDs) {
				Player player =  mgr.getObjectById(Player.class, playerID);
				players.add(player);

				if (player.getGCMID() != null) {
					Sender sender = new Sender(Constant.API_KEY);
					Message.Builder builder = new Message.Builder();
					builder.addData("type", "room");
					builder.addData("roomID", "" + room.getRoomID());

					// send message to corresponding player 3 times
					try {
						sender.send(builder.build(), player.getGCMID(), 3);	
					} catch (IOException iEx) {
						
					}
				}
			}

			// TODO: this is too game specific, we need to move this somewhere
			if (room.isFinished()) {
				// if room is finished, time to update rating
				// get board state
				String boardState = room.getGameState();
				long[] state = new long[9];

				// assume board state is JSON Array formatted
				try {
					JSONArray array = new JSONArray(boardState);

					for (int i = 0; i < array.length(); ++i) {
						state[i] = array.getLong(i);
					}
				} catch (JSONException jEx) {
					
				}

				// check winning state
				long winner = 0;
				for (int i = 0; i < 3; ++i) {
					// check horizontal
					winner |= checkWinner(state, i * 3, i * 3 + 1, i * 3 + 2);
					// check vertical
					winner |= checkWinner(state, i, i + 3, i + 6);
				}
				// check diagonal
				winner |= checkWinner(state, 0, 4, 8);
				winner |= checkWinner(state, 2, 4, 6);

				for (Player player : players) {
					Integer playerRating = player.getRating() == null ? 0 : player.getRating();
					// draw!
					if (winner == 0) {
						player.setRating(playerRating + 1);
					} else if (winner == player.getPlayerID()) {
						player.setRating(playerRating + 3);
					}

					mgr.makePersistent(player);
				}
			}
		} finally {
			mgr.close();
		}
		return room;
	}

	// TODO: this is too game specific. We need to move it somewhere
	private long checkWinner(long[] state, int a, int b, int c) {
		if (state[a] == state[b] && state[b] == state[c]) {
			return state[a];
		}
		return 0;
	}

	/**
	 * This method removes the entity with primary key id.
	 * It uses HTTP DELETE method.
	 *
	 * @param id the primary key of the entity to be deleted.
	 * @return The deleted entity.
	 */
	@ApiMethod(name = "removeRoom")
	public Room removeRoom(@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
		Room room = null;
		try {
			room = mgr.getObjectById(Room.class, id);
			mgr.deletePersistent(room);
		} finally {
			mgr.close();
		}
		return room;
	}

	private boolean containsRoom(Room room) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Room.class, room.getRoomID());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static PersistenceManager getPersistenceManager() {
		return PMF.getInstance().getPersistenceManager();
	}

}
